#!/bin/sh

set -e

cat <<-EOF
===============================================================================

About this build: this rebuild has been done as part of reproduce.debian.net
where we aim to reproduce Debian binary packages distributed via ftp.debian.org,
by rebuilding using the exact same packages as the original build on the buildds,
as described in the relevant .buildinfo file from buildinfos.debian.net.

For more information please go to https://reproduce.debian.net or join
#debian-reproducible on irc.debian.org

===============================================================================

EOF

cd "$(dirname "$1")"
mkdir -p etc/apt
mkdir -p var/lib/apt/lists/
export TMPDIR=/srv/rebuilderd/tmp ; mkdir -p $TMPDIR ; chmod 777 $TMPDIR
echo 'deb-src [signed-by=/usr/share/keyrings/debian-archive-keyring.gpg] https://deb.debian.org/debian trixie main' > etc/apt/sources.list
apt-get -o Dir=. update -q
apt-get -o Dir=. source -qq --print-uris "$(basename "$1" | cut -d_ -f1)"
apt-get -o Dir=. source -qq --download-only "$(basename "$1" | cut -d_ -f1)"
dcmd sha256sum *.dsc

echo "+------------------------------------------------------------------------------+"
echo "| Calling debrebuild                           $(date -u -R) |"
echo "+------------------------------------------------------------------------------+"
echo
echo Rebuilding $(basename "$1") in $(pwd) now.
set -x
nice /usr/bin/debrebuild --buildresult="${REBUILDERD_OUTDIR}" --builder=sbuild+unshare --cache=/srv/rebuilderd/cache -- "${1}"
