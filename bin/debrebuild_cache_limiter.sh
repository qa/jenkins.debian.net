#!/bin/bash
#
# Copyright 2025 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2

CACHE=~rebuilderd/cache

if [ ! -d $CACHE ] ; then
	echo "$CACHE does not exist."
	exit 1
fi

case $HOSTNAME in
	ionos17*)	LIMIT=50  ;;
	codethink*)	LIMIT=20  ;;
	osuosl*)	LIMIT=333 ;;
	infom07*)	LIMIT=200 ;; # FIXME: drop extra partition again?
	infom08*)	LIMIT=100 ;;
	riscv64*)	LIMIT=180 ;;
	*)		echo "Limit for $HOSTNAME not defined."
			exit 1 ;;
esac

# delete 1000 oldest files
# FIXME: use atime, but then, this script should not exist in the first place... :)
find $CACHE -type f -printf '%T+ %p\n' | sort | head -n 1000|cut -d ' ' -f2-|xargs sudo rm 

set -e
set -o pipefail	

SIZE=$(du -sh $CACHE | grep G | cut -d 'G' -f 1)

if [ $SIZE -gt $LIMIT ] ; then
	echo "$CACHE is still ${SIZE}G, reducing further."
	$0
else
	echo "$CACHE is ${SIZE}G, voila."
fi

exit 0
