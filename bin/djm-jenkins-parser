#!/bin/bash
# vim: set noexpandtab:

#
# djm - documented jenkins maintenance logparser for jenkin build logs
#
# Copyright 2023 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2
#

set -e
set -o pipefail		# see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

# check environment
if [ -z "$DJM_USER" ] ; then
	echo "Environment variable DJM_USER must be set. Exiting."
	exit 1
fi
if [ ! -d ~jenkins/jobs ] ; then
	echo "~jenkins/jobs is not a directory, are you running this on the jenkins host?"
	exit 1
fi
cd ~jenkins/jobs

export TZ="/usr/share/zoneinfo/UTC"
MY_CACHE=~/.djm-jenkins-parser.cache
MY_RESULTS=~/.djm-jenkins-ui.log
LOGS=""
ZLOGS=""

find_all_logs() {
	echo "Note: freshly initialized run detected, this will take a few minutes."
	LOGS=$(find ./reproducible_*/builds/*/log 2>/dev/null || true)
	ZLOGS=$(find ./reproducible_*/builds/*/log.gz 2>/dev/null || true)
	touch $MY_CACHE
}

#
# main
#

# find recent / unparsed logfiles
if [ ! -f $MY_CACHE ] ; then
	find_all_logs
elif [ $(cat $MY_CACHE | wc -l) -gt 150000 ] ; then
	figlet "to note:"
	echo "$MY_CACHE has become very big, moving it away."
	mv -v $MY_CACHE ${MY_CACHE}.$(date -u '+%Y-%m-%d')
	find_all_logs
else
	LOGS=$(find ./reproducible_*/builds/*/log -newer $MY_CACHE 2>/dev/null || true)
	ZLOGS=$(find ./reproducible_*/builds/*/log.gz -newer $MY_CACHE 2>/dev/null || true)
fi
echo "Parsing $(echo $LOGS $ZLOGS | sed 's# #\n#g' | wc -l) logfiles now."

# find logfiles triggered by $DJM_USER and write correspondig djm logfile entries
(
for i in $LOGS $ZLOGS ; do
	DIRNAME="$(dirname $i)"
	if [ ! -f $i ] || grep -q "$DIRNAME/log" $MY_CACHE ; then
		: # echo $i already processed, continue.
	else
		echo "$DIRNAME/log" >> $MY_CACHE
		if [ "$(basename $i)" == "log" ] ; then
			RESULT=$(head -1 $i | grep -E -i "Started by user.*$DJM_USER" || true)
		else
			RESULT=$(zcat $i | head -1 | grep -E -i "Started by user.*$DJM_USER" || true)
		fi
		if [ -n "$RESULT" ] ; then
			: # echo $i
			echo "$(stat -c %w $i | cut -d ':' -f1-2) UTC, jenkins web UI, jenkins-ui, job triggered, $(echo $i | cut -d '/' -f2)"
		else
			: #echo autoscheduled: $i
		fi
	fi
done
 ) | sort | tee -a $MY_RESULTS
