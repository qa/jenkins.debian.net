== legacy TODO

=== jenkins-job-builder related

* yaml could be refactored, lots of duplication in there. this seems to be helpful: http://en.wikipedia.org/wiki/YAML#References (pyyaml which jenkins-job-builder uses supports them)

=== debugging job runs should be made easy

----
 <      h01ger> | i think the jenkins-debug-job script should be a python script
 <      h01ger> | and j-j-b or another yaml parser can supply job configuration knowledge to that script
 <      h01ger> | \o/
 <      h01ger> | and that python script can also first determine whether the environment is as needed for the job, and if not, complain verbosely+helpfully and exit
----

== Improve existing tests

=== tests.reproducible-builds.org

=== Debian reproducible builds

* get rid off "set -x # # to debug diffoscoppe/schroot problems"
** add check if package to be build has been blacklisted since scheduling and abort
** on SIGTERM, also cleanup on remote build nodes there! (via ssh &)
** check rbuild logs for "DIFFOSCOPE='E: Failed to change to directory /tmp: Permission denied' and deal with those

* higher prio:
** reenable disorderfs setup, check that it *always* unmounts + cleans up nicely
** pkg pages
*** new table in pkg/test history page: schedule - if that package is currently scheduled
*** add link to pkg set(s) if pkg is member of some
** link pkg sets and issues, that is: at least show packages without issues on pkg set pages, maybe also some issues which need actions (like uninvestigated test failures)
** notes related:
*** #786396: classify issue by "toolchain" or "package" fix needed: show bugs which block a bug
*** new page with annotated packages without categorized issues (and probably without bugs as only note content too, else there are too many)
*** new page with packages that have notes with comments (which are often useful / contain solutions / low-hanging fruits for newcomers)
*** new page with notes that doesn't make sense: a.) packages which are reproducible but should not, packages that build but shouldn't, etc.
*** new page with packages which are reproducible on one arch and unreproducible on another arch (in the same suite, so unstable only atm)
*** new page with packages which ftbfs on one arch and build fine on another arch (in the same suite, so unstable only atm)
*** new page with packages which ftbfs in testing but build fine on sid
*** new page with packages which are orphaned but have a reproducible usertagged patch
*** new page showing arch all packages which are cross-reproducible, and those which are not
** new pages: r.d.n/$maintainer-email redirecting to r.d.n/maintainers/unstable/${maintainer-email}.html, showing the unreproducible packages for that address. and a sunny "yay, thank you"-summary for those with only reproducible packages.
** new page: "open bugs with patches, sorted by maintainers" page and to the navigation, make those NMUable bugs visible
** improve ftbfs page: list packages without bugs and notes first
** bin/_html_indexes.py: bugs = get_bugs() # this variable should not be global, else merely importing _html_indexes always queries UDD
** once firefox 48 is available: revert 1b4dc1b3191e3623a0eeb7cacef80be1ab71d0a2 / grep for _js and remove it…

* lesser prio
** scheduler: check if there have been more than X failures or depwait in the last Y hours and if so unschedule all packages, disable scheduling and send a mail informing us.
** pkg sets related:
*** add new pkg set: torbrowser-build-depends
*** fix essential set: currently it only has the ones explicitly marked Essential:yes; they and their dependencies make up the full "essential closure set" (sometimes also called pseudo-essential)
*** replace bin/reproducible_installed_on_debian.org with a proper data provider from DSA, e.g. https://salsa.debian.org/dsa-team/mirror/debian.org/blob/master/debian/control
** a reproducible_log_grep_by_sql.(py|sh) would be nice, to only grep in packages with a certain status (build in the last X days)
** database issues
*** stats_build table should have package ids, not just src+suite+arch as primary key
*** move "untested" field in stats table too? (as in csv output...)
** blacklist script should tell if a package was already blacklisted. also proper options should be used...
** maintenance.sh: delete the history pages once a page has been removed from all suites+archs
** reproducible.debian.net rename: rgrep all the files…
** debbindiff2diffoscope rename: do s#dbd#ds#g and s#DBD#DS#g and rename dbd directories?
** diffoscope needs to be run on the target arch... (or rather: run on a 64bit architecture for 64bit architectures and on 32bit for 32 bit archs), this should probably be doable with a simple i386 chroot on the host (so using qemu-static to run it on armhf should not be needed, probably.)
** support for arbitrary (to be implemented) Debian-PPAs and external repos, by just giving a source URL
** once stabilized notification emails should go through the package tracker.  The 'build' keyword seems to be the better fit for this.  To do so just send the emails to dispatch@tracker.debian.org, setting "X-Distro-Tracker-Package: foo" and "X-Distro-Tracker-Keyword: build".  This way people wanting to subscribe to our notification don't need to ask us and can do that by themselves.
** repo-comparison: check for binaries without source
** issues: currently only state of amd64 is shown. it would be better to display packages as unreproducible if they are unreproducible on any architecture.
** include diffoscope run time in log
* for _service.sh
** enabling the service in update_jdn
** maintenance job might want to:
*** check for running builds using systemctl show & ps fax
*** check if at least one build is running on a build node
** use ExecStop to kill remote builds on shutdown
*** remove cleanup_nodes.sh once this works
*** add support for disabling archs and for shutdown+respan of workers
*** add support for starting/stopping workers for specific archs
* missing variations:
** 32/64 bit kernel variation on i386
** prebuilder does (user) group variation like this: https://salsa.debian.org/reproducible-builds/reproducible-misc/blob/master/prebuilder/pbuilderhooks/A02_user
** variation of $TERM and $COLUMN (and maybe $LINES), unset in the first run, set to "linux" and "77" (and maybe "42") in the 2nd run.
*** actually TERM is set to "linux" by default already, COLUMN is unset
** vary order of $PATH entries, see #844500
** vary the length of the build paths (/build/first vs /build/second), only once the unreproducibilities caused by different build paths are solved
** vary the init system: sysv and systemd
** vary SSD/HDD on i386?
** vary temp dir variables such as TMP/TMPDIR/TEMP/TEMPDIR/MAGICK_TMPDIR
** maybe vary build with pbuilder and sbuild (but maybe only useful with different setup jobs only…)

==== reproducible Debian armhf

* make systems send mail, use port 465
* rename all the nodes from $HOSTNAME to $HOSTNAME-armhf-rb ?
** we could get rid of the links in jenkins.d.n.git/hosts/
** we could simplify .../hosts/*/etc/munin/munin-node.conf

==== reproducible Debian arm64

* vary DEB_BUILD_OPTS? (NUM_CPU)

==== reproducible Debian installation

* see https://wiki.debian.org/ReproducibleInstalls
* run this as a new job

==== reproducible coreboot

* add more variations: domain+hostname, uid+gid, USER, UTS namespace
* build the docs?
* also build with payloads. x86 use seabios as default, arm boards don't have a default. grub is another payload. and these: bayou  coreinfo  external  filo  libpayload  nvramcui - and:
** CONFIG_PAYLOAD_NONE=y
** CONFIG_PAYLOAD_ELF is not set
** CONFIG_PAYLOAD_LINUX is not set
** CONFIG_PAYLOAD_SEABIOS is not set
** CONFIG_PAYLOAD_FILO is not set
** CONFIG_PAYLOAD_GRUB2 is not set
** CONFIG_PAYLOAD_TIANOCORE is not set
* libreboot ships images, verify those?
* explain status in plain english
* use disorderfs for 2nd build

==== reproducible OpenWrt

* add credit for logo/artwork
* explain status in plain english
* build path variation
* incorporate popular third-party ("external feeds") packages?
* html: build variations are wrong
* html: git commit output includes garbage
* html: css: add some space on the left side

==== reproducible NetBSD

* explain status in plain english
** explain MKREPRO is set to "yes"
** explain MKREPRO_TIMESTAMP set to $SOURCE_DATE_EPOCH
* use disorderfs for 2nd build

==== reproducible FreeBSD

* useful improvements:
** investigate how to use tmpfs on freebsd and build there. see mdmfs(8)
** find a way to be informed about updates and keep it updated - see 'freebsd-update cron' and 'pkg audit'.  The latter is run periodic(8) as part of the nightly root@ emails.
** modify PATH, uid, gid and USER too and host+domainname as well. The VM is only used for this, so we could change the host+domainname temporaily between builds too.
** add freebsd vm as node to jenkins and run the script directly there, saves lot of ssh hassle
** run diffoscope natively

* TODO: random notes, to be moved to README
** we build the freebsd master branch
** we build with sudo too
*** rather not change /usr/obj to be '~jenkins/obj' and build with WITH_INSTALL_AS_USER. also not build in /usr/src. if so, we need to define some variable so we can do so.... but we need a stable path anyway, so whats the point.
*** maybe build as user in /usr/src...
* first build world, later build ports (pkg info...)

* document how the freebsd build VM was set up:
** base 10.1 install following https://www.urbas.eu/freebsd-10-and-profitbricks/
** modified files:
*** /etc/rc.conf
*** /etc/resolv.conf
*** /boot/loader.conf.local
** pkg install screen git vim sudo munin-node poudriere
*** configure /usr/local/etc/munin/munin-node.conf to allow jenkins to access it
*** configure /usr/local/etc/denyhosts.conf and /etc/hosts.allow and touch /etc/hosts.deniedssh
** adduser holger
** adduser jenkins (with bash as default shell)
** adduser mattia
** mkdir -p /srv/reproducible-results
** chown -R jenkins:jenkins /srv/

* system maintenance
** upgraded the VM:
*** done with: 'freebsd-update upgrade -r 10.2' as root in screen
*** and with:  'freebsd-update upgrade -r 10.3'
*** and with:  'freebsd-update upgrade -r 11.0'
*** and with:  'freebsd-update upgrade -r 11.1'
*** and with:  'freebsd-update upgrade -r 11.2' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 12.0' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 12.1' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 12.2' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 13.0' followed by 'pkg-static install pkg ; pkg upgrade; pkg install screen git vim sudo munin-node poudriere'
*** and with:  'freebsd-update upgrade -r 13.1' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 13.2' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 14.0' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 14.1' followed by 'pkg-static install pkg ; pkg upgrade'
*** and with:  'freebsd-update upgrade -r 14.2' followed by 'pkg-static install pkg ; pkg upgrade'
*** and switch to latest branch as described in https://docs.freebsd.org/en/books/handbook/ports/#quarterly-latest-branch
*** with mkdir -p /usr/local/etc/pkg/repos ; echo 'FreeBSD: { url: "pkg+http://pkg.FreeBSD.org/${ABI}/latest" }' > /usr/local/etc/pkg/repos/FreeBSD.conf
*** and with 'pkg update -f ; pkg upgrade'

* online disk resizing howto: https://www.freebsd.org/doc/handbook/disks-growing.html

==== reproducible Fedora

* make sure the pages meet https://fedoraproject.org/wiki/Design/Requirements
 and ask the web design team for help via filing a ticket as described there
* '/var/cache/mock/fedora-23-x86_64/' has three subdirs we need to handle (put on tmpfs, monitor size, clean sometimes): ccache, root_cache and  yum_cache
* '/var/lib/mock' should be put on /srv/workspace aka tmpfs
* setup script:
** mock --clean just uninstalls the chroot but it'll still be rebuilt next time using cache.  you can delete the caches from /var/cache/mock/ or touch the mock config
** is /etc/yum/repos.d/fedora.repo really needed?
** hosts/osuosl1/etc/yum/repos.d/* is really not sooo good but works…
* build script
** cleanup mock cache between two builds: --scrub=all might be too much, but whats sensible (or is it --scrub=all?)?
** no variations introduced yet:
*** use '-j$NUM_CPU' and 'NEW_NUM_CPU=$(echo $NUM_CPU-1|bc)'
*** modify TZ, LANG, LC_ALL, umask
* other bits:
** use modified rpmbuild package from dhiru
** verify gpg signatures (via /etc/mock/)
** one day we will want to schedule all 17k source packages in fedora…
* build rawhide too (once fedora-23 builds nicely), releasever=rawhide

* more notes:
** https://fedoraproject.org/wiki/Using_Mock_to_test_package_builds
** http://miroslav.suchy.cz/blog/archives/2015/05/28/increase_mock_performance_-_build_packages_in_memory/index.html
** manually create a fedora chroot using rpm, wget + yum: http://geek.co.il/2010/03/14/how-to-build-a-chroot-jail-environment-for-centos

==== reproducible qubes

* add qubes test on t.r-b.o
----
        git clone https://github.com/qubesos/qubes-builder
        make get-sources BUILDERCONF=scripts/travis-builder.conf COMPONENTS=installer-qubes-os
        export DIST_DOM0=fc23
        export USE_QUBES_REPO_VERSION=3.2
        export INSTALLER_KICKSTART=/tmp/qubes-installer/conf/travis-iso.ks

        make qubes iso BUILDERCONF=scripts/travis-builder.conf VERBOSE=0 COMPONENTS=installer-qubes-os
----
* depends:  apt install createrepo python-yaml
* once this iso is being tested, it will be interesting to build the Qubes templates as well, as those images (Qubes templates are images) will be copied on the installation iso. the above iso is a stripped down iso without templates… (and not the real thing)

==== reproducible guix

* there's no "apt-get install", because of non-FHS conformance, but see https://www.gnu.org/software/guix/download/
*  there's a privileged build daemon, which is needed to perform fully isolated builds, see https://www.gnu.org/software/guix/manual/html_node/Build-Environment-Setup.html#Build-Environment-Setup
* it's a bit of work to set up, but all the steps are documented. the "binary installation" method being the easiest.
* Manolis wrote:
----
There are two ways to install guix, through prebuilt binaries or through
the source.

*Binary installation:

Go to
<http://www.gnu.org/software/guix/manual/html_node/Binary-Installation.html>,
grab the tarball and follow the instructions there.

*Source installation:

First make sure you have the dependencies mentioned at
<http://www.gnu.org/software/guix/manual/guix.html#Requirements> installed.

Then download Guix's source from
ftp://alpha.gnu.org/gnu/guix/guix-0.9.0.tar.gz and use the usual
./configure && make && make install

After you have Guix built, you need to create the build-users and have
the guix-daemon run as root, as described here
<https://www.gnu.org/software/guix/manual/html_node/Build-Environment-Setup.html>.

Keep in mind that the guix-daemon must always run as root.

*Testing if everything works:

Now just run `guix package -i vim` as a non-root user. If it runs
correctly, Guix is ready for work.
----


=== qa.debian.org*

* udd-versionskew: explain jobs in README
* udd-versionskew: also provide arch-relative version numbers in output too

=== d-i_manual*

* d-i_check_jobs.sh: check for removed manuals (but with existing jobs) missing
* svn:trunk/manual/po triggers the full build, should trigger language specific builds.
* svn:trunk/manual is all that's needed, not whole svn:trunk

=== d-i_build*

* d-i_check_jobs.sh: check for removed package (but with existing jobs) missing
* build packages using jenkins-debian-glue and not with the custom scripts used today?
* run scripts/digress/ ?

=== chroot-installation_*

* use schroot for chroot-installation, stop using plain chroot everywhere

== Debian Packaging related

This setup should ideally come as a Debian source package, one far away day maybe.


// vim: set filetype=asciidoc:
